package com.example.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordRepeat: EditText
    private lateinit var buttonSumbit: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerLisneters()
    }

    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPasswordRepeat = findViewById(R.id.editTextPasswordRepeat)
        buttonSumbit = findViewById(R.id.buttonSumbit)
    }
    private fun registerLisneters(){
        buttonSumbit.setOnClickListener{

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val passwordRepeat = editTextPasswordRepeat.text.toString()



            if(email.isEmpty() || password.isEmpty() || passwordRepeat.isEmpty()) {
                Toast.makeText(this,"შეიყვანეთ ყველა ველი!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9){
                editTextPassword.setError("პაროლის მინიმალური სიგრძე: 9 სიმბოლო!")
                editTextPassword.requestFocus()
                editTextPassword.isEnabled = true
                return@setOnClickListener

            }


            if(password != passwordRepeat) {
                editTextPasswordRepeat.setError("პაროლები არ ემთხვევა ერთმანეთს!")
                editTextPasswordRepeat.requestFocus()
                editTextPasswordRepeat.isEnabled = true
                return@setOnClickListener

            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        Toast.makeText(this,"წარმატებით დარეგისტრირდა!",
                            Toast.LENGTH_SHORT).show()
                    }
                    else { Toast.makeText(this,"ვერ დარეგისტრირდა! მომხმარებელი უკვე " +
                            "არსებობს, ან რაღაცა არასწორედ არის შეტანილი!",
                        Toast.LENGTH_LONG).show()
                    }
                }
        }
    }
}